class ApiResponse {

    constructor() {
        this.data = null;
        this.success = true;
        this.error = '';
        this.status = 200;
    }

    buildResponse() {
        return {
            data: this.data,
            error: this.error,
            success: this.success
        };
    }

    handleError(message, status = 500) {
        this.data = null;
        this.success = false;
        this.status = status;
        this.error = message;
    }
}

module.exports = ApiResponse;
