const db = require('./db');
const shortid = require('shortid');

const getTracks = albumId => {
    return db.get('tracks').filter(track => track.albumId === albumId)
}

const getTrack = id => {
    return db.get('tracks').find({id})
}

const addTracks = (tracks) => {

    const tracksWithIds = tracks.map(track => ({
        id: shortid.generate(),
        ...track
    }));

    db.get('tracks')
        .push(...tracksWithIds)
        .write();

    return tracksWithIds;
}

const addTrack = (albumId, track) => {
    const trackToAdd = {
        id: shortid.generate(),
        ...track
    }
    getTracks(albumId).push(trackToAdd).write();
    return trackToAdd;
}

module.exports = {
    getTrack,
    getTracks,
    addTrack,
    addTracks
};


