const db = require('./db');
const shortid = require('shortid');
const { passwordHasher, passwordValidator } = require('../utils/password');

const registerUser = ({ username, password }) => {

    if (isExistingUser( username )) {
        const error = new Error();
        error.code = 400;
        error.message = `${username} is already registered`;
        throw error;
    }

    const hashData = passwordHasher( user.password );

    db.get('users').push({
        id: shortid.generate(),
        username,
        hashData,
        createdAt: new Date().getTime(),
        updatedAt: null
    });

}

const isExistingUser = (username) => {
    return db.get('users').find({ username }).value()
}
