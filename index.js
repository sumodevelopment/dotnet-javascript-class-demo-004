const express = require('express');
const app = express();
const { PORT = 3000 } = process.env;

//#region ServingHTML 
app.use( express.static(__dirname + '/public') );
app.get('/', (req, res) => {
    res.status(200).sendFile( __dirname + '/public/index.html' );
});
app.get('/about', (req, res)=>{
    res.status(200).sendFile( __dirname + '/public/about.html' );
});
//#endregion

// Enable JSON responses and Getting JSON from the REQ body.
app.use( express.json() );

//#region API
// ALBUMS
app.use( '/v1/albums', require('./routes/albums') );
app.use( '/v1/tracks', require('./routes/tracks') );
//app.use( '/v1/users', require('./routes/users') );
//#endregion 

app.listen(PORT, function() {
    console.log( `Server started on port ${ PORT }` );
});
