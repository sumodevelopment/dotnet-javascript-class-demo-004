const ApiResponse = require('../models/api-response');
const router = require('express').Router();
const { getTracks, addTracks } = require('../models/tracks');

router.get('/:albumId', (req, res)=> {

    const response = new ApiResponse();
    const { albumId } = req.params;

    try {
        response.data = getTracks(albumId);
    } catch(error) {
        response.handleError(error.message);
    }

    return res.status( response.status ).json( response );
});

router.post('/', (req, res) =>{

    const response = new ApiResponse();
    const { tracks } = req.body;

    try {
        response.data = addTracks(tracks);
    } catch (error) {
        response.handleError(error.message);
    }

    return res.status(response.status).json( response.buildResponse() );

});

module.exports = router;
