const { getAllAlbums, getAlbumById, addAlbum } = require("../models/albums");
const ApiResponse = require('../models/api-response');
const router = require('express').Router();

router.get('/',  (req, res) => {
    let response = new ApiResponse();
    try {
        response.data = getAllAlbums();
    } catch (error) {
       response.handleError(error.message);
    }
    return res.status(response.status).json(response.buildResponse())
});

router.get('/:albumId',  (req, res) => {
    let response = new ApiResponse();
    const { albumId } = req.params;

    try {
        response.data = getAlbumById( albumId );
    } catch (error) {
        response.handleError(error.message);
    }

    return res.status(response.status).json( response.buildResponse() );
});

router.post('/',  (req, res) => {

    let response = new ApiResponse();
    const { album } = req.body;

    try {
        response.data = addAlbum( album );
    } catch (error) {
        response.handleError(error.message, error.code);
    }

    return res.status( response.status ).json( response.buildResponse() );

});

module.exports = router;













