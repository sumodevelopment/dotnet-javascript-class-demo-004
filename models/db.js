const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('db.json')
const db = low(adapter)

if (!db.getState().hasOwnProperty('albums')) {
    const DEFAULTS = { albums: [],  tracks: [],  users: [] };
    db.defaults( DEFAULTS ).write();
}

module.exports = db;
