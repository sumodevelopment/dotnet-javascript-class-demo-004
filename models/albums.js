const db = require('./db');
const shortid = require('shortid');

const getAllAlbums = () => {
    return db.get('albums');
}

const getAlbumById = id => {
    return getAllAlbums().find({ id });
}

const getAlbumByTitle = title => {
    return getAllAlbums().find({ title });
}

const addAlbum = album => {

    const { title } = album;
    const existingAlbum = getAlbumByTitle(title).value();

    if (existingAlbum) {
        const error = new Error();
        error.code = 400;
        error.message = `The album ${title} is already in the catalogue`;
        throw error;
    }

    const albumToAdd = {
        id: shortid.generate(),
        ...album
    };
    getAllAlbums().push(albumToAdd).write();
    return albumToAdd;
}

module.exports = {
    getAllAlbums,
    getAlbumById,
    getAlbumByTitle,
    addAlbum
};
